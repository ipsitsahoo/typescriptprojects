import {User} from "./models/User"
import axios from 'axios';

const user = new User({id: 1});
// axios.post('http://localhost:3000/users', {
//     name: 'myname',
//     age: 20
// });
user.events.on('change', () => {
    console.log("change!");
});

user.events.trigger('change');