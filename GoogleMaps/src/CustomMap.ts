import 'googlemaps';

// Instruction to every other class
// that how they can use this class or adding markers.

export interface Mappable {
    location: {
        lat: number;
        lng: number;
    };
    markerContent(): string;
    color:string;
}

export class CustomMap {
    private googleMap: google.maps.Map;

    constructor(id: string) {
        this.googleMap = new google.maps.Map(document.getElementById(id), {
            zoom: 1,
            center: {
                lat: 0,
                lng: 0
            }
        });
    }

    addMapMarker(mappable: Mappable) : void {
        let marker = new google.maps.Marker({
            map: this.googleMap,
            position: {
                lat: mappable.location.lat,
                lng: mappable.location.lng
            }
        });
        marker.addListener('click', () => {
            const infoWindow = new google.maps.InfoWindow({
                content: mappable.markerContent()
            });
            infoWindow.open(this.googleMap, marker);
        });
    };
}