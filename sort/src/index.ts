import { Sorter } from "./Sorter";
import { CharactersCollection } from "./CharactersCollection";
import {NumbersCollection} from "./NumbersCollection"

const charactersCollection = new CharactersCollection("Ipsit");
console.log(charactersCollection.data);
let sort = new Sorter(charactersCollection);
sort.sort();
console.log(charactersCollection.data);

const numberCollection = new NumbersCollection([ 10, 9, 8, 7 ,6]);
console.log(numberCollection.data);
sort = new Sorter(numberCollection);
sort.sort();
console.log(numberCollection.data);
