interface Reportable {
    summary() : string;
};
const oldCivic = {
    name: 'civic',
    year: 2000,
    isBroken: true,
    summary(): string {
        return `Awesome detailing for ${this.name}`;
    }
};

const drink = {
    color: 'brown',
    carbonated: true,
    sugar: 40,
    summary(): string {
        return `My drink has ${this.sugar} amount in it.`
    }
};
const printSummary = (item: Reportable) : void => {
    console.log(item.summary());
};

printSummary(oldCivic);
printSummary(drink);