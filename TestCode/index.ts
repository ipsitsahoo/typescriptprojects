let num: number = 10;
let colors: string[];

colors[0] = "blue";

const add = (a: number, b: number): number => {
    return a + b;
};

const carmakers = ["ford", "toyota", "suzuki"];

// Inference to extract values
const car = carmakers[0];
const myCar = carmakers.pop();

// prevent incompatible values
carmakers.push("hyundai");

// Use map
carmakers.map((car: string): string => {
    // car as a string is now recognized in map.
    return car;
});

// Flexible Types
const importantDates: (Date| string)[] = [new Date()];
importantDates.push("2010-03-01");
importantDates.push(new Date());

// Understanding Tuples
const carSpecs: [number, number] = [400, 3354];

const carStats = {
    horsepower: 400,
    weight: 3354
};