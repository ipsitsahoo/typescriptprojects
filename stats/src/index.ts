import { MatchResult } from "./Utils";
import { MatchReader } from "./MatchReader";

const matches = new MatchReader("./football.csv");
matches.read();

let manUnitedWins = 0;

// Iterate through our collection of matches
for (let match of matches.data)
{
    if (match[1] === 'Man United' && match[5] === MatchResult.HomeWin) {
        manUnitedWins++;
    }
    else if (match[2] === 'Man United' && match[5] === MatchResult.AwayWin) {
        manUnitedWins++;
    }
}

console.log(`Man United wins ${manUnitedWins} matches`);