"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Utils_1 = require("./Utils");
var MatchReader_1 = require("./MatchReader");
var matches = new MatchReader_1.MatchReader("./football.csv");
matches.read();
var manUnitedWins = 0;
// Iterate through our collection of matches
for (var _i = 0, _a = matches.data; _i < _a.length; _i++) {
    var match = _a[_i];
    if (match[1] === 'Man United' && match[5] === Utils_1.MatchResult.HomeWin) {
        manUnitedWins++;
    }
    else if (match[2] === 'Man United' && match[5] === Utils_1.MatchResult.AwayWin) {
        manUnitedWins++;
    }
}
console.log("Man United wins " + manUnitedWins + " matches");
